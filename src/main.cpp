#include <iostream>
#include <libpq-fe.h>
#include <cstring>
#include <iomanip>

#include "affichage.h"

int main()
{
  PGPing code_retour_ping;
  PGresult *resultat;
  code_retour_ping = PQping("host=postgresql.bts-malraux72.net port=5432");

  if(code_retour_ping == PQPING_OK)
  {
    PGconn *connection;
    std::cout << "La connexion au serveur de base de données a été établie avec les paramètres suivants : " << std::endl;
    connection = PQconnectdb("host=postgresql.bts-malraux72.net port=5432 dbname=s.jia user=s.jia connect_timeout=10");
    ConnStatusType conn_status;
    conn_status = PQstatus(connection);

    if(conn_status == CONNECTION_OK)
    {
      affichage_connexion(connection);
      std::cout << "Connexion OK" << std::endl;
      /*    char *user = PQuser(connection);
      char *pass = PQpass(connection);
      char *db = PQdb(connection);
      char *host = PQhost(connection);
      char *port = PQport(connection);
      bool ssl = PQsslInUse(connection);
      int protocol = PQprotocolVersion(connection);
      int server = PQserverVersion(connection);
      std::cout << "* utilisateur : " << user << std::endl;
      std::cout << "* mot de passe : ";

      for(int i = 0; i < sizeof(pass); ++i)
      {
        std::cout << "*";
      }

      std::cout << std::endl;
      std::cout << "* base de données : " << db <<  std::endl;
      std::cout << "* port TCP :" << port << std::endl;

      if(ssl == true)
      {
        std::cout << "* chiffrement SSL : true" << std::endl;
      }
      else
      {
        std::cout << "* chiffrement SSL : false" << std::endl;
      }

      std::cout << "* version du protocole : " << protocol << std::endl;
      std::cout << "* version du serveur :" << server << std::endl;
      std::cout << "* version de la bibliothèque 'libpq' du client : " << PQlibVersion() << std::endl;
      */
      resultat = PQexec(connection, "SELECT \"Animal\".id,\"Animal\".nom AS \"nom de l'animal\",\"Animal\".sexe,\"Animal\".date_naissance AS \"date de naissance\",\"Animal\".commentaires,\"Race\".nom,\"Race\".description FROM si6.\"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura'");
      ExecStatusType status;
      status = PQresultStatus(resultat);

      if(status == PGRES_TUPLES_OK)
      {
        std::cout << "Fin avec succès d'une commande renvoyant des données (telle que SELECT ou SHOW)" << std::endl;
        int nblignes = PQntuples(resultat);
        int nbcolonnes = PQnfields(resultat);
        const char *trait = " | ";
        const char *ligne = "-";
        int max = 0;

        // Normalisation de la largeur des champs à la longueur de champ le plus long
        for(int i = 0; i < nbcolonnes; ++i)
        {
          char *longueur = PQfname(resultat, i);

          if((int)strlen(longueur) > max)
          {
            max = std::strlen(longueur);
          }
        }

        // Ligne de séparation entre les données
        ligne_separation(nbcolonnes, max, ligne);

        // Affichage de l'en-tête présentant le nom de chacun des champs de la requête
        for(int e = 0; e < nbcolonnes; ++e)
        {
          std::cout << trait << std::left << std::setw(max) << PQfname(resultat, e);
        }

        std::cout << std::endl;
        ligne_separation(nbcolonnes, max, ligne);

        // Affichage les données
        for(int l = 0; l < nblignes; l++)
        {
          for(int c = 0; c < nbcolonnes; ++c)
          {
            int maximum = std::strlen(PQgetvalue(resultat, l, c));
            char *description = PQgetvalue(resultat, l, c);

            if(max < maximum)
            {
              description[max] = '\0';

              for(int d = max - 3; d < max; d++)
              {
                description[d] = '.';
              }
            }

            std::cout << trait << std::setw(max) << std::left << PQgetvalue(resultat, l, c);
          }

          std::cout << trait;
          std::cout << std::endl;
        }

        ligne_separation(nbcolonnes, max, ligne);
        std::cout << "L'exécution de la requête SQL a retourné " << nblignes << " enregistrements." << std::endl;
      }
      else if(status == PGRES_EMPTY_QUERY)
      {
        std::cerr << "La chaîne envoyée au serveur était vide." << std::endl;
      }
      else if(status == PGRES_COMMAND_OK)
      {
        std::cout << "Fin avec succès d'une commande ne renvoyant aucune donnée." << std::endl;
      }
      else if(status == PGRES_COPY_OUT)
      {
        std::cout << "Début de l'envoi d'un flux de données." << std::endl;
      }
      else if(status == PGRES_COPY_IN)
      {
        std::cout << "Début de la réception d'un flux de données." << std::endl;
      }
      else if(status == PGRES_BAD_RESPONSE)
      {
        std::cout << "La réponse du serveur n'a pas été comprise." << std::endl;
      }
      else if(status == PGRES_NONFATAL_ERROR)
      {
        std::cout << "Une erreur non fatate est survenue." << std::endl;
      }
      else if(status == PGRES_FATAL_ERROR)
      {
        std::cout << "Une erreur fatale est survenue." << std::endl;
      }
      else if(status == PGRES_COPY_BOTH)
      {
        std::cout << "Lancement du transfert de données Copy In/Out." << std::endl;
      }
      else if(status == PGRES_SINGLE_TUPLE)
      {
        std::cout << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante. Ce statut n'intervient que lorsque le mode ligne-à-ligne a été selectionné pour cette requête." << std::endl;
      }
    }
    else if(conn_status == CONNECTION_BAD)
    {
      std::cerr << "Connexion impossible au serveur" << std::endl;
    }
    else
    {
      std::cerr << "Malheureusement le serveur n'est pas joignable. Vérifier la connectivité" << std::endl;
    }
  }

  return 0;
}
