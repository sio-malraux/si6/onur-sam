#include <libpq-fe.h>
#include <iostream>
#include <iomanip>
#include <cstring>

#include "affichage.h"

void affichage_connexion(PGconn *connection)
{
  char *user = PQuser(connection);
  char *pass = PQpass(connection);
  char *db = PQdb(connection);
  char *host = PQhost(connection);
  char *port = PQport(connection);
  bool ssl = PQsslInUse(connection);
  int protocol = PQprotocolVersion(connection);
  int server = PQserverVersion(connection);
  std::cout << "La connexion au serveur de base de données '" << host << "' a été établie avec les paramètres suivants :" << std::endl;
  std::cout << "* utilisateur :" << user << std::endl;
  std::cout << "* mot de passe :";

  for(size_t i = 0; i < sizeof(pass); ++i)
  {
    std::cout << "*";
  }

  std::cout << std::endl;
  std::cout << "* base de données :" << db <<  std::endl;
  std::cout << "* port TCP :" << port << std::endl;
  std::cout << "* chiffrement SSL :" << ssl << std::endl;

  if(ssl == 1)
  {
    std::cout << "Chiffrement SSL : true" << std::endl;
  }
  else
  {
    std::cout << "Chiffrement SSL : false" << std::endl;
  }

  std::cout << "* version du protocole :" << protocol << std::endl;
  std::cout << "* version du serveur :" << server << std::endl;
  std::cout << "* version de la bibliothèque 'libpq' du client :" << PQlibVersion() << std::endl;
}

void ligne_separation(int nbcolonnes, int max, const char *ligne)
{
  for(int nbseparation = 0; nbseparation < nbcolonnes * (max + 3); ++nbseparation)
  {
    std::cout << ligne;
  }

  std::cout << std::endl;
}

